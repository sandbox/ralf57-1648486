CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Use case
 * How it works
 * Warning


INTRODUCTION
------------

Current Maintainer: Donato Rotunno <donato.rotunno@liip.ch>

The Node Secret Preview module allows the editors to generate a preview url for a node.
This url can be accessed by anyone so that also people outside of the website (non users) can check it out.


USE CASE
--------

The new content (node) has to be checked by an external revisor before publishing.
So:

1) The editor creates a new content
2) The editor emails the preview url to content revisor for approval.
3) The revisor gives the editor/administrator feedback about the new content
4) The editor/administrator takes some action (edit or publish the content).


HOW IT WORKS
------------
After creating a new node, the editor can copy the preview url from the node editing form (Page Preview Url section).
The url looks like this:
https://www.your-site.com/node/1/preview/80669f1eeaad7f4367b6d9831e913f11
and it contains the nid and a token, generated using the nid and the website hash salt.

Following the example above, when the revisor visits the url, the module rebuilds the token using the nid
and, if it matches the token in the url, access is granted and the node is rendered. 


WARNING
-------

Since the module uses the "$drupal_hash_salt" config, it's important not to change its value otherwise the 
old links (the ones you have already communicated) will not work.
If you don't have any pending preview urls then you're safe.
